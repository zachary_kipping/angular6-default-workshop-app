export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: View[];
}

export interface View {
  age: number;
  region: string;
  date: string;
}
